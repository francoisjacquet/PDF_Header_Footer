��    
      l      �       �      �   .        3     :     A     V  +   [     �  '   �    �     �  *   �                    .  ,   5     b  $   s                            
      	           Bottom Margin (mm) Exclude PDF generated using the "Print" button Footer Header PDF Header Footer %s Save The plugin configuration has been modified. Top Margin (mm) You're not allowed to use this program! Project-Id-Version: PDF Header Footer plugin for RosarioSIS
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-08-22 07:35+0200
Last-Translator: François Jacquet <info@rosariosis.org>
Language-Team: RosarioSIS <info@rosariosis.org>
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-KeywordsList: dgettext:2
X-Poedit-Basepath: /var/www/github/rosariosis/plugins/PDF_Header_Footer
X-Generator: Poedit 3.3.2
X-Poedit-SearchPath-0: .
 Spodnji rob (mm) Generirarj PDF datoteko z gumbom "Natisni" Noga Glava Noga/Glava PDF %s Shrani Konfiguracija vtičnika je bila spremenjena. Zgornji rob (mm) Nimate pravic za uporabo aplikacije! 