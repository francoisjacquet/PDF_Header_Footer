��          \      �       �      �   .   �                     4     I    Y     v  .   �     �  
   �  +   �  "         #                                       Bottom Margin (mm) Exclude PDF generated using the "Print" button Footer Header Hide PDF Header and Footer PDF Header Footer %s Top Margin (mm) Project-Id-Version: PDF Header Footer plugin for RosarioSIS
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-01-18 15:05+0100
Last-Translator: François Jacquet <info@rosariosis.org>
Language-Team: RosarioSIS <info@rosariosis.org>
Language: es_ES
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-KeywordsList: ;dgettext:2
X-Poedit-Basepath: ../../..
X-Generator: Poedit 3.2.2
X-Poedit-Bookmarks: -1,-1,3,-1,-1,-1,-1,-1,-1,-1
X-Poedit-SearchPath-0: .
 Margen Inferior (mm) Excluir PDF generados con el botón "Imprimir" Pie de página Encabezado Esconder el encabezado y pie de página PDF Encabezado y Pie de página PDF %s Margen Superior (mm) 