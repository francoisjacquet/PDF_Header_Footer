Plugin Encabezado y pie de página PDF Premium
=============================================

![screenshot](https://www.rosariosis.org/wp-content/uploads/2025/03/pdf-header-footer-premium-screenshot-es.webp)

CARACTERÍSTICAS
---------------

- Códigos de substitución para insertar, de manera dinámica, los datos de la institución (práctico en un entorno con múltiples instituciones), el número de página, el título del documento o la fecha corriente.
- Encabezado y pie de página por programa. Por ejemplo, define un encabezado y un pie de página personalizado para el programa _Calificaciones > Boletines de Calificaciones_.
- [PDF de ejemplo](https://www.rosariosis.org/wp-content/uploads/2025/03/pdf-header-footer-premium-ejemplo.pdf)

[![additional-screenshot](https://www.rosariosis.org/wp-content/uploads/2025/03/pdf-header-footer-premium-screenshot-es-300.webp)](https://www.rosariosis.org/wp-content/uploads/2025/03/pdf-header-footer-premium-screenshot-es.webp) [![additional-screenshot](https://www.rosariosis.org/wp-content/uploads/2025/03/pdf-header-footer-premium-screenshot2-es-300.webp)](https://www.rosariosis.org/wp-content/uploads/2025/03/pdf-header-footer-premium-screenshot2-es.webp)

### [➭ Cambiar a Premium](https://www.rosariosis.org/es/plugins/pdf-header-footer/#premium-plugin)
