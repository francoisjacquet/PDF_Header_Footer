Plugin En-tête et pied de page des PDF
======================================

![screenshot](https://www.rosariosis.org/wp-content/uploads/2025/03/pdf-header-footer-premium-screenshot-fr.webp)

CARACTÉRISTIQUES
----------------

- Codes de substitution pour insérer dynamiquement les informations de l'école (pratique dans une configuration avec plusieurs écoles), le numéro de la page, le titre du document ou bien la date courante.
- En-tête et pied de page par programme. Par exemple, définissez un en-tête et un pied de page personnalisé pour le programme _Notes > Bulletins de notes_.
- [PDF d'exemple](https://www.rosariosis.org/wp-content/uploads/2025/03/pdf-header-footer-premium-exemple.pdf)

[![additional-screenshot](https://www.rosariosis.org/wp-content/uploads/2025/03/pdf-header-footer-premium-screenshot-fr-300.webp)](https://www.rosariosis.org/wp-content/uploads/2025/03/pdf-header-footer-premium-screenshot-fr.webp) [![additional-screenshot](https://www.rosariosis.org/wp-content/uploads/2025/03/pdf-header-footer-premium-screenshot2-fr-300.webp)](https://www.rosariosis.org/wp-content/uploads/2025/03/pdf-header-footer-premium-screenshot2-fr.webp)

### [➭ Passer à Premium](https://www.rosariosis.org/fr/plugins/pdf-header-footer/#premium-plugin)
