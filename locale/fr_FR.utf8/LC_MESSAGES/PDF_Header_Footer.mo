��          \      �       �      �   .   �                     4     I  �  Y     D  2   [     �     �  %   �     �     �                                       Bottom Margin (mm) Exclude PDF generated using the "Print" button Footer Header Hide PDF Header and Footer PDF Header Footer %s Top Margin (mm) Project-Id-Version: PDF Header Footer plugin for RosarioSIS
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-01-18 15:06+0100
Last-Translator: François Jacquet <info@rosariosis.org>
Language-Team: RosarioSIS <info@rosariosis.org>
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-KeywordsList: ;dgettext:2
X-Poedit-Basepath: ../../..
X-Generator: Poedit 3.2.2
X-Poedit-SearchPath-0: .
 Marge inférieure (mm) Exclure PDF générés grâce au bouton "Imprimer" Pied de page En-tête Cacher l'en-tête et pied de page PDF En-tête et pied de page PDF %s Marge supérieure (mm) 