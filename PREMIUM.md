PDF Header Footer Premium plugin
================================

![screenshot](https://www.rosariosis.org/wp-content/uploads/2025/03/pdf-header-footer-premium-screenshot.webp)

FEATURES
--------

- Substitution codes to dynamically insert school information (useful for multi-school setup), page number, document title or current date.
- Per program Header / Footer. For example, define a custom header and footer for the _Grades > Report Cards_ program.
- [Example PDF](https://www.rosariosis.org/wp-content/uploads/2025/03/pdf-header-footer-premium-example.pdf)

[![additional-screenshot](https://www.rosariosis.org/wp-content/uploads/2025/03/pdf-header-footer-premium-screenshot-300.webp)](https://www.rosariosis.org/wp-content/uploads/2025/03/pdf-header-footer-premium-screenshot.webp) [![additional-screenshot](https://www.rosariosis.org/wp-content/uploads/2025/03/pdf-header-footer-premium-screenshot2-300.webp)](https://www.rosariosis.org/wp-content/uploads/2025/03/pdf-header-footer-premium-screenshot2.webp)

### [➭ Switch to Premium](https://www.rosariosis.org/plugins/pdf-header-footer/#premium-plugin)
